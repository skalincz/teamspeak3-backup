#!/bin/bash

#ARGUMENTS
server=$1

#VARIABLES
backupFolder='/home/'
folder='/home/backup/ts3/'
backupServerFolder=$folder$server
backupedFile="ts3server.sqlitedb"
backupedFolder='/home/ts3/'
backupedServerFolder=$backupedFolder$server
backupTimeStamp=`date +"%y-%m-%d-%H"`
backupName="$backupTimeStamp.tar"
fileDest=$backupedFolder$server
fileDestination="$fileDest/$backupedFile"
date=`date +"%y-%m-%d %H:%M"`
logDate="[$date]:"

#Argument checking

if [ -z $1 ]; then
	echo "Usage: ./ts3_backup.sh SERVER";
	echo "$logDate ERROR: No server selected!" >> /var/log/backuplog.txt;
        echo "$logDate Error occured during backing up the server. Ending backup now!" >> /var/log/backuplog.txt;
	echo -e "-----------------------------------------------------------------------\n-----------------------------------------------------------------------" >> /var/log/backuplog.txt;
	exit 0;
fi


#Integrity checking

checkBackupFolder=`ls $backupFolder | grep -x "backup" | nl | awk '{printf $1}'`
# Creating /home/backup directory
if [ "$checkBackupFolder" != "1" ]; then
	echo "$logDate Creating folder home/backup" >> /var/log/backuplog.txt;
	mkdir /home/backup/;
	backupFolder+="backup/"
fi

checkBackupFolder=`ls $backupFolder | grep -x "ts3" | nl | awk '{printf $1}'`

# Creating /home/backup/ts3 directory
if [ "$checkBackupFolder" != "1" ]; then
        echo "$logDate Creating folder home/backup/ts3" >> /var/log/backuplog.txt;
	mkdir /home/backup/ts3;
fi

checkBackupTS3Folder=`ls $folder | grep -x $1 | nl | awk '{printf $1}'`

if [ "$checkBackupTS3Folder" != "1" ]; then
        echo "$logDate Creating folder home/backup/ts3/ARGUMENT" >> /var/log/backuplog.txt;
	mkdir $backupServerFolder
fi

checkBackupFile=`ls $backupedServerFolder | grep -x $backupedFile | nl | awk '{printf $1}'`

if [ "$checkBackupFile" != "1" ]; then
	echo "$logDate ERROR: There is nothing to backup of server $1!" >> /var/log/backuplog.txt;
	echo "$logDate Error occured during backing up the server. Ending backup now!" >> /var/log/backuplog.txt;
	echo -e "-----------------------------------------------------------------------\n-----------------------------------------------------------------------" >> /var/log/backuplog.txt;
	exit 0;
fi

echo "$logDate Starting the backup process of TS3 server called: $1!" >> /var/log/backuplog.txt;
cd $fileDest;
tar -cf $backupName $backupedFile;

checkTaredFile=`ls | grep -e $backupName | nl | awk '{printf $1}'`
if [ "$checkTaredFile" != "1" ]; then
        echo "$logDate ERROR: Error occured during taring the file of server $1!" >> /var/log/backuplog.txt;
        echo "$logDate Error occured during backing up the server. Ending backup now!" >> /var/log/backuplog.txt;
	echo -e "-----------------------------------------------------------------------\n-----------------------------------------------------------------------" >> /var/log/backuplog.txt;
        exit 0;
fi
mv $backupName $backupServerFolder

checkMovedFile=`ls $backupServerFolder | grep -e $backupName | nl | awk '{printf $1}'`
if [ "$checkMovedFile" != "1" ];then
        echo "$logDate ERROR: Error occured during moving the file of server $1!" >> /var/log/backuplog.txt;
        echo "$logDate Error occured during backing up the server. Ending backup now!" >> /var/log/backuplog.txt;
	echo -e "-----------------------------------------------------------------------\n-----------------------------------------------------------------------" >> /var/log/backuplog.txt;
        exit 0;
fi

echo "$logDate Finished backing up the server $1" >> /var/log/backuplog.txt;
echo -e "-----------------------------------------------------------------------\n-----------------------------------------------------------------------" >> /var/log/backuplog.txt;

exit 0;

